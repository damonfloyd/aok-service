import { Server, Request, ResponseToolkit } from "@hapi/hapi";
import * as laabr from 'laabr';
import * as Blipp from 'blipp';
import Jwt from '@hapi/jwt';
// @ts-ignore
import * as SECRETS from './secrets/config.json';
import { AUTH_CONFIG } from './utils/Tokens';

import { routes } from './routes';

const laabrOpts = {
  formats: { onPostStart: ':time :start :level :message' },
  tokens: { 
    start:  () => '[start]',
    payload: (p) => {
      if(p.payload && p.payload.pass) {
        return '[payload]'
      }
      return p.payload;
    },
  },
  indent: 0,
  colored: true,
};

const init = async () => {

  const server: Server = new Server({
      port: 3000,
      host: 'localhost',
      routes: {
        cors: {
          origin: ['http://localhost:8080'],
          additionalHeaders: ['x-aok-user']
        }
      }
  });

  // register plugins
  await server.register({
    plugin: laabr,
    options: laabrOpts,
  });
  await server.register({
    plugin: Blipp, 
    options: { showAuth: true }
  });
  await server.register(Jwt);
  server.auth.strategy("jwt", "jwt", AUTH_CONFIG);
  server.auth.default('jwt');

  // add all the routes.
  routes.forEach((route: any) => {
    server.route(route);
  });

  await server.start();
  console.log('Server running LIKE A BAWSE on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

init();
