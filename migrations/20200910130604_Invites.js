
exports.up = function(knex) {
  return knex.schema.dropTableIfExists('UserInvitations')
    .createTable('UserInvitations', (ui) => {
      ui.increments();
      ui.string('invitee_email', 128).unique().notNullable();
      ui.string('invite_token', 64).unique();
      ui.dateTime('send_date').notNullable().defaultTo(knex.fn.now());
      ui.integer('sender_id').references('id').inTable('Users').unsigned();
      ui.boolean('expired').defaultTo(false);
      ui.boolean('accepted').defaultTo(false);
      ui.dateTime('accept_date');
    });
};

exports.down = function(knex) {
  return knex.schema
    .dropTableIfExists('UserInvitations');
};
