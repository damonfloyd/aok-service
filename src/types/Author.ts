export interface Author {
  id: number;
  name: string;
  color: string;
  animal: string;
  code: string;
  arcana: string;
  suit: string;
  rank: string;
}