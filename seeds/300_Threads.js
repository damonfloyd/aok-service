const faker = require('faker');
require('dotenv').config();

// faker.seed(27548);

exports.seed = async function(knex, Promise) {
  const threads = [{
    id: 1,
    authorId: 2,
    forumId: 1,
    title: 'Here is the lone test thread',
  }];

  if(process.env.NODE_ENV === 'development') {
    for(let i = 0; i < 200; i++) {
      let authorId = Math.floor(Math.random() * 50) + 2;
      let forumId = Math.floor(Math.random() * 8) + 1;
      let title = faker.lorem.sentence();
      threads.push({title, authorId, forumId});
    }
  }
  // Deletes ALL existing entries
  return knex('Threads').del().then(() => {
    return knex('Threads').insert(threads);
  });
};
