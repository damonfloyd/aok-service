import Knex from 'knex';
import path from 'path';

const knex = Knex({
  client: 'sqlite3',
  connection: {
    filename: `${__dirname}/aok-dev.sqlite3`,
  },
  debug: true,
  useNullAsDefault: true
});

export { knex }; 