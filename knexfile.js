// const config = require('./secrets/config.json');
require('dotenv').config();

module.exports = {

  development: {
    client: "sqlite3",
    connection: {
      filename: `${__dirname}/aok-dev.sqlite3`
    },
    migrations: { tableName: 'aok-migrations' },
    seeds: { tableName: './seeds' },
    debug: true,
    useNullAsDefault: true
  },

  staging: {
    client: "sqlite3",
    connection: {
      filename: `${__dirname}/aok-dev.sqlite3`
    },
    migrations: { tableName: 'aok-migrations' },
    seeds: { tableName: './seeds' },
    debug: true,
    useNullAsDefault: true
  },

  // production: {
  //   client: "postgresql",
  //   connection: {
  //     database: "my_db",
  //     user: "username",
  //     password: "password"
  //   },
  //   pool: {
  //     min: 2,
  //     max: 10
  //   },
  //   migrations: {
  //     tableName: "knex_migrations"
  //   }
  // }

};
