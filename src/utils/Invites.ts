import * as randtoken from 'rand-token';
import DataClient from '../data/dataclient';
import moment from 'moment';

const create = async (senderId: number, email: string) => {
  const db = await DataClient.create();
  const where = {
    invitee_email: email,
    expired: false,
    accepted: false
  };
  const [check] = await db('UserInvitations').select('*').where(where).limit(1);
  if(check) {
    return '__ALREADY_INVITED__'
  }
  const record = {
    invitee_email: email,
    invite_token: randtoken.generate(40),
    send_date: moment.utc().toISOString(),
    sender_id: senderId,
  };
  const result = await db('UserInvitations').insert(record);
  return record.invite_token;
}

const accept = async(invite_token: string): Promise<string> => {
  const db = await DataClient.create();
  const [record] = await db('UserInvitations').where({invite_token});
  if(record) {
    const id = record.id;
    const sendDate = moment.utc(record.send_date);
    const now = moment.utc();
    if(now.diff(sendDate, 'days') > 14 || record.expired) {
      // token has expired
      if(!record.expired) {
        await db('UserInvitations').update({expired: true}).where({id});
      }
      return '__TOKEN_EXPIRED__';
    }
    else if(record.accepted) {
      return '__TOKEN_ALREADY_USED__';
    }
    else {
      const update = {
        accepted: true,
        accept_date: now.toISOString(),
      };
      await db('UserInvitations').update(update).where({id});
      return '__TOKEN_ACCEPTED__';
    }
  }
  return "__TOKEN_NOT_FOUND__";
}

export {
  accept,
  create,
}