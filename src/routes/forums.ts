import { Request, ResponseToolkit } from '@hapi/hapi';
import DataClient from '../data/dataclient';
import { Forum } from '../types';
import { THREADS_PER_PAGE } from '../constants';

const defaultOpts = {
  inactive: false,
  hidden: false,
};

const forumsRoutes = [
  {
    method: 'GET',
    path: '/forums',
    options: { cors: true },
    handler: async (request: Request, h: ResponseToolkit) => {
      const db = await DataClient.create();
      // get all except for inactive/hidden
      const forums: Forum[] = await db('Forums')
                            .where(defaultOpts)
                            .select('*')
                            .orderBy('index');
      return h.response(forums).type('text/json').code(200);
    }
  },
  {
    method: 'GET',
    path: '/category/{categoryId}/forums',
    options: { cors: true },
    handler: async (request: Request, h: ResponseToolkit) => {
      let categoryId = request.params.categoryId;
      const db = await DataClient.create();
      const opts = {categoryId, ...defaultOpts};
      const forums: Forum[] = await db('Forums')
                            .where(opts)
                            .select('*')
                            .orderBy('index');
      return h.response(forums).type('text/json').code(200);
    }
  },
  {
    method: 'GET',
    path: '/forum/{id}',
    options: { cors: true },
    handler: async (request: Request, h: ResponseToolkit) => {
      let id = request.params.id;
      const db = await DataClient.create();
      const forums = await db('Forums')
                            .where({id})
                            .select('*')
                            .orderBy('index')
                            .limit(1);
      let forum: Forum;
      if(Array.isArray(forums)) {
        forum = forums[0];
      }
      else {
        forum = forums;
      }
      const threadCount = (await db('Threads').count('id as count').where({forumId: id}))[0];
      forum.threadCount = parseInt(threadCount.count as string);
      let pages = Math.floor(forum.threadCount / THREADS_PER_PAGE);
      if(forum.threadCount % THREADS_PER_PAGE > 0) {
        pages++;
      }
      forum.pages = pages;
      return h.response(forum).type('text/json').code(200);
    }
  },
];

export { forumsRoutes };
