const faker = require('faker');
const moment = require('moment');
require('dotenv').config();

const d = moment.utc().subtract(1, 'day');  // yesterday

exports.seed = function(knex) {
  console.log('"' + process.env.NODE_ENV + '"');
  if(process.env.NODE_ENV === 'development') {
    console.log('>>>>>>>> DEVELOPMENT <<<<<<<<<');
    // Deletes ALL existing entries
    return knex('Posts').del()
      .then(async function () {
      const threads = await knex('Threads').select('id', 'authorId');
      for await (let t of threads) {
        let created_at = d.toISOString();
        let updated_at = created_at;
        const posts = [
          {authorId: t.authorId, threadId: t.id, content: faker.lorem.paragraph(), created_at, updated_at},
        ];
        
        for (let i = 2; i < (Math.floor(Math.random() * 40) + 25); i++) {
          let authorId = Math.floor(Math.random() * 50) + 2;
          let threadId = t.id;
          let content = faker.lorem.paragraph();
          d.add(2, 'seconds');
          created_at = d.toISOString();
          updated_at = created_at;

          posts.push({authorId, threadId, content, created_at, updated_at});
        }
        await knex('Posts').insert(posts);
      }
      // make sure thread 1 has at least 7 pages (to ensure pager works)
      const threadOnePosts = []
      for(let i = 0; i < 126; i++) {
        let authorId = Math.floor(Math.random() * 50) + 2;
        let threadId = 1;
        let content = faker.lorem.paragraph();
        d.add(2, 'seconds');
        created_at = d.toISOString();
        updated_at = created_at;

        threadOnePosts.push({authorId, threadId, content, created_at, updated_at});
      }
      await knex('Posts').insert(threadOnePosts);
    });
  }
  else if(process.env.NODE_ENV === 'staging') {
    const posts = [
      {
        authorId: 2,
        threadId: 1,
        content: '...and here is the lone post in it.',
        created_at: d.toISOString(),
        updated_at: d.toISOString(),
      }
    ];
    return knex('Posts').del().then(async() => {
      await knex('Posts').insert(posts);
    });
  }
};
