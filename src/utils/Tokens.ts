import Jwt from '@hapi/jwt';
//@ts-ignore
import * as SECRETS from '../../secrets/config.json';
import DataClient from '../data/dataclient';
import * as Sessions from '../utils/Sessions';
import moment from 'moment';
// import { userRoutes } from '../routes/users';

const SECONDS_IN_DAY = 60 * 60 * 24;

const expireOldSessions = async () => {
  const db = await DataClient.create();
  return await db('Sessions').where('expires_at', '<', db.fn.now()).del();
}

const userHasSession = async (user_guid: string) => {
  const db = await DataClient.create();
  const sesh = (await db('Sessions').where({ user_guid }).select('*').limit(1))[0];
  if(sesh === '') {
    return false;
  }
  return true;
}

const sign = (scope: string[], guid: string) => {
  const expirationDate = new Date(0);
  expirationDate.setUTCSeconds(Date.now() / 1000);
  // two weeks from now:
  expirationDate.setUTCSeconds(SECONDS_IN_DAY * 14);
  const claims = {
      iat: Date.now(),
      exp: expirationDate.valueOf() / 1000,
      scope,
      iss: SECRETS.site_domain,
      guid
  };
  return Jwt.token.generate(claims, SECRETS.token_key, { algorithm: 'HS256' });
}

const validate = async (artifacts, request, h) => {
  let isValid = false;
  const credentials = artifacts.decoded.payload;
  // console.log('TOKEN:', artifacts.decoded.payload);
  // clear old sessions
  expireOldSessions();
  // see if user has session
  isValid = await userHasSession(credentials.guid);
  if(isValid) {
    const seen = await Sessions.updateLastSeen(credentials.guid);
  }
  return {isValid, credentials};
}

const AUTH_CONFIG = {
  keys: SECRETS.token_key,
  // fields that needs to be verified and respective values
  verify: {
    aud: false,
    sub: false,
    // issuer of the jwt
    iss: SECRETS.site_domain,
    // check expiry - default true
    exp: true,
  },
  // token validation fn gets executed after token signature verification
  validate,
}


export {
    AUTH_CONFIG,
    expireOldSessions,
    userHasSession,
    sign,
    validate,
}