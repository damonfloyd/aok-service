exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('Colors').del()
    .then(function () {
      // Inserts seed entries
      return knex('Colors').insert([
        {name: 'Red'},
        {name: 'Orange'},
        {name: 'Yellow'},
        {name: 'Green'},
        {name: 'Blue'},
        {name: 'Purple'},
        {name: 'Indigo'},
        {name: 'Teal'},
        {name: 'Violet'},
        {name: 'Gray'},
      ]);
    });
};
