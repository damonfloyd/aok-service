
exports.up = function(knex, Promise) {
  return knex.schema.dropTableIfExists('Users')
  .createTable('Users', (u) => {
    u.increments();
    u.string('name', 64).unique().notNullable();
    u.string('passwd', 256).notNullable();
    u.string('email', 256).notNullable();
    u.string('guid', 64).notNullable().unique();
    u.boolean('isAdmin').defaultTo('false');
    u.dateTime('join_date').notNullable().defaultTo(knex.fn.now());
    u.dateTime('last_seen').defaultTo(null);
    u.boolean('needs_reset').defaultTo(false);
    u.timestamp('last_login');
    u.string('reset_token', 40);
    u.timestamp('reset_expires_at');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('Users');
};
