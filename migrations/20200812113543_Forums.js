
exports.up = function(knex, Promise) {
  return knex.schema
    .dropTableIfExists('Categories')
    .createTable('Categories', (c) => {
      c.increments();
      c.string('name', 128).unique().notNullable();
      c.integer('index').unsigned();
    })
    .dropTableIfExists('Forums')
    .createTable('Forums', (f) => {
      f.increments();
      f.string('name', 128).unique().notNullable();
      f.string('description', 512);
      f.integer('index').unsigned();
      f.boolean('inactive').defaultTo(false);
      f.boolean('hidden').defaultTo(false);
      f.integer('categoryId').references('id').inTable('Categories').unsigned();
      f.integer('parentId').references('id').inTable('Forums').unsigned();
    })
    .dropTableIfExists('Threads')
    .createTable('Threads', (t) => {
      t.increments();
      t.string('title', 256);
      t.integer('forumId').references('id').inTable('Forums').unsigned();
      t.integer('authorId').references('id').inTable('Users').unsigned();
      t.timestamps(true, true);
    })
    .dropTableIfExists('Posts')
    .createTable('Posts', (p) => {
      p.increments();
      p.integer('authorId').references('id').inTable('Users').unsigned();
      p.integer('threadId').references('id').inTable('Threads').unsigned();
      p.text('content', 'mediumtext');
      p.timestamps(true, true);
    });
};

exports.down = function(knex, Promise) {
  return knex.schema
    .dropTableIfExists('Posts')
    .dropTableIfExists('Threads')
    .dropTableIfExists('Forums')
    .dropTableIfExists('Categories');
};
