export interface Forum {
  id: number;
  name: string;
  index: number;
  inactive: boolean;
  hidden: boolean;
  categoryId: number;
  parentId: number | null;
  subforums?: Forum[];
  threadCount?: number;
  pages?: number;
}
