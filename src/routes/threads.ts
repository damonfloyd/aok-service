import { Request, ResponseToolkit } from '@hapi/hapi';
import DataClient from '../data/dataclient';
import { getAuthor } from '../utils/GetAuthor';
import { POSTS_PER_PAGE, THREADS_PER_PAGE } from '../constants';

const threadRoutes = [
  {
    method: 'GET',
    path: '/forum/{forumId}/threads',
    options: { cors: true },
    handler: async (request: Request, h: ResponseToolkit) => {
      const db = await DataClient.create();
      const forumId = request.params.forumId;
      let page = 1;
      let offset = POSTS_PER_PAGE;
      if(request.query.page) {
        page = parseInt(Array.isArray(request.query.page) ? request.query.page[0] : request.query.page);
        offset = (page - 1) * POSTS_PER_PAGE;
      }
      const threadList = [];
      
      const threads = await db('Threads')
        .select('Threads.id', 'Threads.title', 'Threads.forumId', 'Threads.authorId', 'Users.name as authorName')
        .where({forumId})
        .leftJoin('Users', 'Users.id', 'Threads.authorId')
        .orderBy('created_at', 'desc')
        .limit(THREADS_PER_PAGE)
        .offset(offset);

      for await (let t of threads) {
        let threadInfo = (await db('Posts')
          .select('Posts.authorId', 'Posts.created_at', 'Users.name as authorName')
          .leftJoin('Users', 'Users.id', 'Posts.authorId')
          .where('Posts.threadId', t.id)
          .orderBy('Posts.created_at', 'desc')
          .limit(1))[0];
        
        const author = await getAuthor(t.authorId);
        const lastPostAuthor = await getAuthor(threadInfo.authorId);
        
        author.name = t.authorName;
        lastPostAuthor.name = threadInfo.authorName;
        delete t.authorId;
        delete t.authorName;
        delete threadInfo.authorName;

        t.author = author;
          // doing this within the above query breaks it....somehow...
        let postCount = await db('Posts').count('id as totalPostCount').where({threadId: t.id});

        let totalPostCount = parseInt(postCount[0].totalPostCount as string);

        let pages = Math.floor(totalPostCount / POSTS_PER_PAGE);
        if(totalPostCount % POSTS_PER_PAGE > 0) {
          pages++;
        }

        const lastPost = {
          author: lastPostAuthor, 
          createdAt: threadInfo.created_at,
        };

        threadList.push(Object.assign(t, {totalPostCount}, {pages}, {lastPost}));
      }

      return h.response(threadList).type('text/json').code(200);
    }
  },
  {
    method: 'GET',
    path: '/thread/{id}',
    options: {cors: true},
    handler: async (request: Request, h: ResponseToolkit) => {
      const db = await DataClient.create();
      const id = request.params.id;
      const threads = await db('Threads')
        .select('Threads.id', 'Threads.title', 'Threads.forumId', 'Threads.authorId', 'Users.name as authorName')
        .where({'Threads.id': id})
        .leftJoin('Users', 'Users.id', 'Threads.authorId')
        .limit(1);

      let threadRaw;
      if(Array.isArray(threads)) {
        threadRaw = threads[0];
      }
      else {
        threadRaw = threads;
      }
      let threadInfo = await db('Posts')
        .select('Posts.authorId', 'Posts.created_at', 'Users.name as authorName')
        .leftJoin('Users', 'Users.id', 'Posts.authorId')
        .where('Posts.threadId', threadRaw.id)
        .orderBy('Posts.created_at', 'desc')
        .limit(1);
      
        // doing this within the above query breaks it....somehow...
      let postCount = await db('Posts').count('id as totalPostCount').where({threadId: threadRaw.id});

      let totalPostCount = parseInt(postCount[0].totalPostCount as string);

      let pages = Math.floor(totalPostCount / POSTS_PER_PAGE);
      if(totalPostCount % POSTS_PER_PAGE > 0) {
        pages++;
      }

      const lastPost = {
        authorId: threadInfo[0].authorId, 
        createdAt: threadInfo[0].created_at,
        authorName: threadInfo[0].authorName,
      };
      const thread = Object.assign(threadRaw, {totalPostCount}, {pages}, {lastPost});
      return h.response(thread).type('text/json').code(200);
    }
  },
  {
    method: 'POST',
    path: '/forum/{forumId}/thread',
    options: {cors: true },
    handler: async (request: Request, h: ResponseToolkit) => {
      const db = await DataClient.create();
      const forumId = request.params.forumId;
      const { authorId, title, content } = request.payload as unknown as { authorId: number, title: string, content: string };
      const thread = await db('Threads').insert({forumId, authorId, title});
      let threadId = -1;
      if(Array.isArray(thread)) {
        threadId = thread[0];
      }
      else {
        threadId = thread;
      }
      const newPost = { threadId, authorId, content };
      const post = await db('Posts').insert(newPost);

      return h.response(thread).type('text/json').code(200);
    }
  },
];

export { threadRoutes };