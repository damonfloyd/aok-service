import DataClient from '../data/dataclient';
import { Author } from '../types';

const authorColumns = [
  'Users.id',
  'Users.name',
  'UserProfiles.id',
  'UserProfiles.code',
  'Colors.name as color',
  'Animals.name as animal',
  'Suits.name as suit',
  'Ranks.name as rank', 
  'Arcana.name as arcana',
];

export const getAuthor = async (id: number): Promise<Author> => {
  const db = await DataClient.create();
  const authorRaw = (
    await db('Users')
      .select(authorColumns)
      .where({'Users.id': id})
      .leftJoin('UserProfiles', 'Users.id', 'UserProfiles.userId')
      .leftJoin('Colors', 'UserProfiles.colorId', 'Colors.id')
      .leftJoin('Animals', 'UserProfiles.animalId', 'Animals.id')
      .leftJoin('Arcana', 'UserProfiles.arcanaId', 'Arcana.id')
      .leftJoin('Suits', 'UserProfiles.suitId', 'Suits.id')
      .leftJoin('Ranks', 'UserProfiles.rankId', 'Ranks.id')
      .limit(1)
    )[0];
  return authorRaw;
}



