import DataClient from '../data/dataclient';
import moment from 'moment';
const GUID = require('uuid');

const createUserSession = async (user_guid) => {
  const db = await DataClient.create();
  // delete any old session we have for this user
  let seshDelete = await db('Sessions').where({user_guid}).del();
  // create a user session:
  const session_id = GUID.v4();
  const started_at = moment.utc().toISOString();
  const expires_at = moment.utc().add(2, 'weeks').toISOString();
  let session = await db('sessions')
    .insert({ user_guid, session_id, started_at, expires_at });
  return session;
}

const updateLastLogin = async (guid) => {
  // console.log('>>>>>>>>>>>>>>>>>>>>>> update last login:', guid);
  const db = await DataClient.create();
  const last_login = moment.utc().toISOString();
  let loginUpdate = await db('Users').where({guid}).update({last_login});
  return loginUpdate;
}

const updateLastSeen = async (guid) => {
  const db = await DataClient.create();
  const last_seen = moment.utc().toISOString();
  let seenUpdate = await db('Users').where({guid}).update({last_seen});
  return seenUpdate;
}

const getSessionByUserGuid = async (user_guid) => {
  const db = await DataClient.create();
  const sesh = await db('Sessions').where({user_guid}).select('*');
  return sesh;
}

export { 
  createUserSession,
  updateLastLogin,
  updateLastSeen,
  getSessionByUserGuid
};