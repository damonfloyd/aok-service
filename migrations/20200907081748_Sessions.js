
exports.up = function(knex, Promise) {
  return knex.schema.dropTableIfExists('Sessions')
  .createTable('Sessions', function(sessionsTable) {
    sessionsTable.string('user_guid', 64).references('guid').inTable('Users');     // user id
    sessionsTable.string('session_id', 64);  // guid for the session itself
    sessionsTable.timestamp('started_at').notNullable();
    sessionsTable.timestamp('expires_at').notNullable();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('Sessions');
};