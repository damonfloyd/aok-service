
exports.seed = async function(knex, Promise) {
  // Deletes ALL existing entries
  await knex('Categories').del();
  return knex('Categories').insert([
    {id: 1, name: 'Main', index: 0},
    {id: 2, name: 'Discussion', index: 1},
    {id: 3, name: 'Community', index: 2},
  ]);
};
