export interface NewUser {
  username: string;
  password: string;
  email: string;
  code: string;
  animalId: number;
  colorId: number;
}