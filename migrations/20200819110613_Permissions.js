
exports.up = function(knex) {
  return knex.schema
  .dropTableIfExists('UserCategoryPerms')
  .createTable('UserCategoryPerms', (ucp) => {
    ucp.increments();
    ucp.integer('userId').references('id').inTable('Users').unsigned();
    ucp.integer('categoryId').references('id').inTable('Categories').unsigned();
    ucp.boolean('visible').defaultTo('false');
    ucp.unique(['userId', 'categoryId']);
  })
  .createTable('UserForumPerms', (ufp) => {
    ufp.increments();
    ufp.integer('userId').references('id').inTable('Users').unsigned();
    ufp.integer('forumId').references('id').inTable('Forums').unsigned();
    ufp.boolean('reader').defaultTo('false');
    ufp.boolean('poster').defaultTo('false');
    ufp.boolean('moderator').defaultTo('false');
    ufp.unique(['userId', 'forumId']);
  });
};

exports.down = function(knex) {
  return knex.schema
    .dropTableIfExists('UserCategoryPerms')
    .dropTableIfExists('UserForumPerms');
};
