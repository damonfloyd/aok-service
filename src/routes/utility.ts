import { Request, ResponseToolkit } from "@hapi/hapi";
import DataClient from '../data/dataclient';

export const utilityRoutes = [
  {
    method: 'GET',
    path: '/animals',
    options: { cors: true, auth: false },
    handler: async (request: Request, h: ResponseToolkit) => {
      const db = await DataClient.create();
      const animals = await db('Animals').select('*').orderBy('popularity', 'asc');
      // remove the 5 most popular
      animals.splice(-5);
      return h.response(animals).code(200);
    }
  },
  {
    method: 'GET',
    path: '/colors',
    options: { cors: true, auth: false },
    handler: async (request: Request, h: ResponseToolkit) => {
      const db = await DataClient.create();
      const colors = await db('Colors').select('*').orderBy('popularity', 'asc');
      // remove the 2 most popular
      colors.splice(-2);
      return h.response(colors).code(200);
    }
  }
];
