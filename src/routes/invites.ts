import { Request, ResponseToolkit } from '@hapi/hapi';
import DataClient from '../data/dataclient';
import * as Invites from '../utils/Invites';
import ServiceResponse from '../types';
import * as Emails from '../utils/Email';

export const inviteRoutes = [
  {
    method: 'POST',
    path: '/invite',
    options: { cors: true, auth: false },
    handler: async (request: Request, h: ResponseToolkit) => {
      const db = await DataClient.create();
      const {sender, email} = request.payload as {sender, email};
      const token = await Invites.create(sender, email);
      if(token === '__ALREADY_INVITED__') {
        return h.response({message: 'user already invited'}).code(409).takeover();
      }
      // create mail message and send it
      Emails.sendInviteEmail(email, token);
      return h.response({message: 'invite has been sent!'}).code(200);
    }
  },
  {
    method: 'GET',
    path: '/accept/{token}',
    options: { cors: true, auth: false },
    handler: async (request: Request, h: ResponseToolkit) => {
      const db = await DataClient.create();
      const token = request.params.token;
      const acceptResult = await Invites.accept(token);
      return h.response({message: acceptResult}).code(200);
    }
  }
]