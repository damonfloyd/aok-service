import nodemailer from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import randtoken from 'rand-token';

const ACTIVATE_URL = 'http://localhost:8080/accept/%%TOKEN%%';
// const RESET_URL = 'http://localhost:4200/reset/%%TOKEN%%';

// TODO: change this config once we have hosting set up.ß
const smtpConfig: SMTPTransport.Options = {
    // pool: true,
    host: 'mail.stacksofblackwax.com',
    port: 587,
    auth: {
        user: 'sobw_mail@stacksofblackwax.com',
        pass: 'omgf34r'
    },
    tls: {
        rejectUnauthorized: false
    }
};

const transporter = nodemailer.createTransport(smtpConfig);

function sendMail(message: Mail.Options) {
    transporter.sendMail(message, (error: Error, info: any) => {
    if(error) {
        return console.error(error);
    }
    //console.log('Message %s sent: %s', info.messageId, info.response);
    });
}


const sendInviteEmail = (toAddress: string, token: string) => {
    let robot = randtoken.generate(5);
    let mailOptions: Mail.Options = {
        from: `"🤖 Stacks Bot ${ robot }" <DO_NOT_REPLY@armyofkindness.org>`,
        to: toAddress,
        subject: 'You Have Been Invited!!',
        text: '',
        html: ''
    };

    let activateUrl = ACTIVATE_URL.replace('%%TOKEN%%', token);
    let textMsg = `
    You are receiving this email because a current member of the Army of Kindness forums
    has invited you.  If you do not wish to join, you can simply ignore this email. Otherwise,
    click the link below to accept the invite.

    ${ activateUrl }
    
    This mailbox is unmonitored.  Please do not reply to it.
    
    Always be kind!`;

    let htmlMsg = `
    <h2>Bleep bloop blerrrrrp!</h2>
    <p>
    You are receiving this email because a current member of the Army of Kindness forums
    has invited you.  If you do not wish to join, you can simply ignore this email. Otherwise,
    click the link below to accept the invite.
    <br/>
    <br/>
    <a href="${ activateUrl }">${ activateUrl }</a><br/>
    <br/>
    This mailbox is unmonitored.  Please do not reply to it.<br/>
    <br/>
    Always be kind!<br/>
    </p>`;

    mailOptions.text = textMsg;
    mailOptions.html = htmlMsg;
    sendMail(mailOptions);
    return;
};

export {
  sendInviteEmail,
}
