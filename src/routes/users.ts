import { Request, ResponseToolkit } from "@hapi/hapi";
import * as Passwords from '../utils/Passwords';
import * as Sessions from '../utils/Sessions';
import * as Tokens from '../utils/Tokens';
import DataClient from '../data/dataclient';
import { addUser } from './users/addUser';

const userFields = [
  'id',
  'name',
  'passwd',
  'guid',
  'isAdmin',
]

const userRoutes = [
  {
    method: 'POST',
    path: '/login',
    options: { cors: true, auth: false },
    handler: async (request: Request, h: ResponseToolkit) => {
      const db = await DataClient.create();
      const { user, pass } = request.payload as { user: string, pass: string };
      const incorrect = { message: 'username or password is incorrect'};
      // get user by username
      const userCheck = (await db('Users').where({name: user}).select(userFields).limit(1))[0];
      // match password
      if(userCheck) {
        const passMatched = await Passwords.matchPassword(pass, userCheck.passwd);
        if(passMatched) {
          const sesh = await Sessions.createUserSession(userCheck.guid);
          const updateLogin = await Sessions.updateLastLogin(userCheck.guid);
          const scope = ['user'];
          if(userCheck.isAdmin) {
            scope.push('admin');
          }
          const token = Tokens.sign(scope, userCheck.guid);
          const user = {
            token,
            id: userCheck.id,
            guid: userCheck.guid,
            isAdmin: userCheck.isAdmin,
          };
          return h.response(user).type('text/json').code(200).takeover();
        }
      }

      return h.response(incorrect).type('text/json').code(401);
    }
  },
  {
    method: 'GET',
    path: '/user/{id}',
    options: { cors: true },
    handler: async (request: Request, h: ResponseToolkit) => {
      const db = await DataClient.create();
      const id = request.params.id;
      const userColumns = [
        'id', 'name', 'email', 'isAdmin', 
        'join_date as joinDate', 'last_seen as lastSeen',
      ];
      const userRaw = (await db('Users')
        .where({id}).select(userColumns)
        .limit(1))[0];

      const profileColumns = [
        'UserProfiles.id',
        'UserProfiles.tagline',
        'UserProfiles.code',
        'Colors.name as color',
        'Animals.name as animal',
        'Suits.name as suit',
        'Ranks.name as rank', 
        'Arcana.name as arcana',
        'UserProfiles.rating',
        'UserProfiles.created_at as createdAt',
        'UserProfiles.hideEmail',
        'UserProfiles.allowMessages',
      ];

      const profile = (await db('UserProfiles')
        .where({userId: id})
        .select(profileColumns)
        .leftOuterJoin('Suits', 'UserProfiles.suitId', 'Suits.id')
        .leftOuterJoin('Ranks', 'UserProfiles.rankId', 'Ranks.id')
        .leftOuterJoin('Arcana', 'UserProfiles.arcanaId', 'Arcana.id')
        .leftOuterJoin('Colors', 'UserProfiles.colorId', 'Colors.id')
        .leftOuterJoin('Animals', 'UserProfiles.animalId', 'Animals.id')
        .limit(1))[0];

      const user = Object.assign(userRaw, {profile});

      return h.response(user).type('text/json').code(200);
    },
  },
  {
    method: 'GET',
    path: '/user/{id}/postinfo',
    options: { cors: true },
    handler: async (request: Request, h: ResponseToolkit) => {
      const id = request.params.id;
      const db = await DataClient.create();
      const cols = [
        'Posts.content', 'Posts.created_at as createdAt', 'Threads.title as thread',
        'Threads.id as threadId', 'Threads.forumId'
      ];
      const lastPost = (await db('Posts')
                        .select(cols)
                        .where({'Posts.authorId': id})
                        .leftOuterJoin('Threads', 'Threads.id', 'Posts.threadId')
                        .orderBy('Posts.created_at', 'DESC')
                        .limit(1))[0];
      const postCount = (await db('Posts').count('id as postCount').where({authorId: id}))[0];
      const postinfo = Object.assign({}, {lastPost}, postCount);

      return h.response(postinfo).type('text/json').code(200);
    },
  },
  {
    method: 'GET',
    path: '/author/{id}',
    options: { cors: true },
    handler: async (request: Request, h: ResponseToolkit) => {
      const db = await DataClient.create();
      const id = request.params.id;
      const selectFields = {
        id: 'Users.id',
        name: 'Users.name',
        color: 'Colors.name',
        animal: 'Animals.name',
        code: 'UserProfiles.code',
        arcana: 'Arcana.name',
        suit: 'Suits.name',
        rank: 'Ranks.name',
      };
      const authorRaw = await db('Users').select(selectFields).where({'Users.id': id})
        .leftJoin('UserProfiles', 'Users.id', 'UserProfiles.userId')
        .leftJoin('Colors', 'UserProfiles.colorId', 'Colors.id')
        .leftJoin('Animals', 'UserProfiles.animalId', 'Animals.id')
        .leftJoin('Arcana', 'UserProfiles.arcanaId', 'Arcana.id')
        .leftJoin('Suits', 'UserProfiles.suitId', 'Suits.id')
        .leftJoin('Ranks', 'UserProfiles.rankId', 'Ranks.id')
        .limit(1);

      let author = {};
      if(Array.isArray(authorRaw)) {
        author = authorRaw[0];
      }
      else {
        author = authorRaw;
      }
      
      return h.response(author).type('text/json').code(200);
    },
  },
];

userRoutes.push(addUser);

export { userRoutes };

