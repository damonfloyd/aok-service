import Knex from 'knex';
import path from 'path';

async function create() {
  const knex = Knex({
    client: "sqlite3",
    connection: {
      filename: `aok-dev.sqlite3`
    },
    debug: true,
    useNullAsDefault: true
  });

  // console.log(__dirname);

  // verify connection works 
  try {
    await knex.raw('SELECT 0 WHERE 0');
    return knex;
  }
  catch (error) {
    console.log(error);
    throw new Error(`Can't connect to db.  Ensure your connection params are sound.`);
  }
}

export default { create };