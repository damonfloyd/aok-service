import DataClient from '../data/dataclient';

// returns the id of a random suit, excluding the most popular one (so 1 of 3)
const getRandomSuit = async():Promise<number> => {
  const db = await DataClient.create();
  const suits = await db('Suits').select('*').orderBy('popularity', 'asc');
  suits.splice(-1);
  const chosen = suits[~~(suits.length * Math.random())];
  return chosen.id as number;
}

const getRandomArcana = async():Promise<number> => {
  const db = await DataClient.create();
  const arcana = await db('Arcana').select('*').orderBy('popularity', 'asc');
  arcana.splice(-4);  // remove the 4 most popular
  const chosen = arcana[~~(arcana.length * Math.random())];
  return chosen.id as number;
}

const getRandomRank = async():Promise<number> => {
  const db = await DataClient.create();
  const ranks = await db('Ranks').select('*').whereNot({name: '__ARCANA__'}).orderBy('popularity', 'asc');
  ranks.splice(-3);
  const chosen = ranks[~~(ranks.length * Math.random())];
  return chosen.id as number;
}

const updatePopularity = async (table: string, id: number) => {
  const db = await DataClient.create();
  const update = await db(table).where({id}).increment('popularity', 1);
}

export {
  getRandomArcana,
  getRandomRank,
  getRandomSuit,
  updatePopularity,
}