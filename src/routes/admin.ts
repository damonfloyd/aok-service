import { Request, ResponseToolkit } from "@hapi/hapi";
import DataClient from '../data/dataclient';

const adminRoutes = [
  {
    method: 'GET',
    path: '/admin/animalscolors',
    options: { cors: true, auth: { access: { scope: ['admin']}} },
    handler: async (request: Request, h: ResponseToolkit) => {
      const db = await DataClient.create();
      const animals = await db('Animals').select('*').orderBy('name');
      const colors = await db('Colors').select('*').orderBy('name');
      const all = Object.assign({}, {animals, colors});
      return h.response(all).type('text/json').code(200);
    },
  },
];

export { adminRoutes };