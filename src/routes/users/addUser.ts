import { Request, ResponseToolkit } from "@hapi/hapi";
import DataClient from '../../data/dataclient';
import * as GUID from 'uuid';
import * as Passwords from '../../utils/Passwords';
import * as SuitsArcana from '../../utils/SuitsArcana';
import { NewUser } from '../../types';
import moment from 'moment';

export const addUser = {
  method: 'POST',
  path: '/user',
  options: { cors: true, auth: false },
  handler: async (request: Request, h: ResponseToolkit) => {
    const db = await DataClient.create();
    const { username, password, email, animalId, colorId, code } = request.payload as NewUser;
    const passwd = Passwords.hashPassword(password);

    const user = {
      name: username, 
      passwd: await Passwords.hashPassword(password),
      email,
      guid: GUID.v4(),
      isAdmin: false,
    };
    try {
      const [uResult] = await db('Users').insert(user);
      const profile = {
        userId: uResult,
        colorId,
        animalId,
        code,
        suitId: await SuitsArcana.getRandomSuit(),
        rankId: await SuitsArcana.getRandomRank(),
        arcanaId: await SuitsArcana.getRandomArcana(),
        tagline: '',
        created_at: moment.utc().toISOString(),
        updated_at: moment.utc().toISOString(),
      };
      const [upResult] = await db('UserProfiles').insert(profile);
      const popUps = [
        {table: 'Colors', id: colorId},
        {table: 'Animals', id: animalId},
        {table: 'Suits', id: profile.suitId},
        {table: 'Ranks', id: profile.rankId},
        {table: 'Arcana', id: profile.arcanaId},
      ];

      for await (let pu of popUps) {
        await SuitsArcana.updatePopularity(pu.table, pu.id);
      }

      return h.response({message: 'user created', username}).code(200).takeover();
    }
    catch(e) {
      return h.response({message: e}).code(500).takeover();
    }
  }
}