
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('UserForumPerms').del();
    // .then(function () {
    //   // Inserts seed entries
    //   return knex('UserForumPerms').insert([
    //     {userId: 2, forumId: 1, reader: true},
    //     {userId: 2, forumId: 2, reader: true, poster: true},
    //     {userId: 2, forumId: 3, reader: true},
    //     {userId: 2, forumId: 4, reader: true},
    //     {userId: 2, forumId: 5, reader: true},
    //     {userId: 2, forumId: 6, reader: true},
    //     {userId: 2, forumId: 7, reader: true},
    //     {userId: 3, forumId: 1, reader: true, poster: true},
    //     {userId: 3, forumId: 2, reader: true, poster: true},
    //     {userId: 3, forumId: 3, reader: true, poster: true },
    //     {userId: 3, forumId: 4, reader: true, poster: true },
    //     {userId: 3, forumId: 5, reader: true, poster: true },
    //     {userId: 3, forumId: 6, reader: true, poster: true },
    //     {userId: 3, forumId: 7, reader: true, poster: true },
    //     {userId: 4, forumId: 1, reader: true, poster: true, moderator: true },
    //   ]);
    // });
};
