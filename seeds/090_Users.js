const faker = require('faker');
const GUID = require('uuid');
const moment = require('moment');
require('dotenv').config({ path: `./.env.${process.env.NODE_ENV}` })

faker.seed(27548);

const generateCode = () => {
  // random number between 1 and 9999 inclusive
  let rnd = Math.floor(Math.random() * (9999 - 1 + 1)) + 1;
  let str = rnd + '';
  let pad = '0000';
  return pad.substring(0, pad.length - str.length) + str
}

exports.seed = async function(knex, Promise) {
  const colors = await knex('Colors').select('*');
  const animals = await knex('Animals').select('*');
  const created_at = moment().toISOString();
  const updated_at = moment().toISOString();

  const users = [
    {
      id: 1, 
      name: 'ptah',
      guid: GUID.v4(),
      email: 'ptah.raziel@protonmail.com',
      passwd: '$2b$10$dOGlbg226.vqx7Q9eI4EVeq0UJ2WqA0dm3TDEhhWWJ352BjZAo9dG',
      isAdmin: true,
    },
    {
      id: 2, 
      name: 'PurpleFerret9907',
      guid: GUID.v4(),
      email: 'ptah.raziel@protonmail.com',
      passwd: '$2b$10$dOGlbg226.vqx7Q9eI4EVeq0UJ2WqA0dm3TDEhhWWJ352BjZAo9dG',
      isAdmin: false,
    }
  ];

  const profiles = [
    {
      userId: 1,
      tagline: 'When in doubt - be kind.  When not in doubt - be kind',
      suitId: 2,
      rankId: 15,
      arcanaId: 6,
      colorId: 6,
      animalId: 17,
      code: '9907',
      created_at,
      updated_at,
    },
    {
      userId: 2,
      tagline: 'When in doubt - be kind.  When not in doubt - be kind',
      suitId: 2,
      rankId: 15,
      arcanaId: 6,
      colorId: 6,
      animalId: 17,
      code: '9907',
      created_at,
      updated_at,
    }
  ];

  console.log(`>>>>>>>>>>>>>>>>>>>>>>> ./.env.${process.env.NODE_ENV} <<<<<<<<<<<<<<<<<<<<<<<`);
  if(process.env.NODE_ENV === 'development') {
    // generate 49 more users
    for(let i = 0; i < 49; i++) {
      let color = colors[Math.floor(Math.random() * colors.length)];
      let animal = animals[Math.floor(Math.random() * animals.length)];
      let code = generateCode();
      let user = {
        id: i + 3,
        name: `${color.name}${animal.name}${code}`,
        guid: GUID.v4(),
        email: faker.internet.email(),
        passwd: faker.internet.password(),
        isAdmin: false,
      };
      users.push(user);

      let profile = {
        userId: i + 3,
        tagline: faker.random.words(10),
        suitId: Math.floor(Math.random() * 4) + 1,
        rankId: Math.floor(Math.random() * 14) + 1,
        arcanaId: Math.floor(Math.random() * 22) + 1,
        colorId: color.id,
        animalId: animal.id,
        code,
        created_at,
        updated_at,
      };
      profiles.push(profile);
    }
  }
  // Deletes ALL existing entries
  return knex('Users').del()
    .then(function () {
      // Inserts seed entries
      return knex('Users').insert(users)
      .then(() => {
        return knex('UserProfiles').insert(profiles);
      });
    });
};
