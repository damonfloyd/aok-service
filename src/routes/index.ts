import { Request, ResponseToolkit } from "@hapi/hapi";
import { userRoutes } from './users';
import { categoryRoutes } from './categories';
import { forumsRoutes } from './forums';
import { threadRoutes } from './threads';
import { postRoutes } from './posts';
import { adminRoutes } from './admin';
import { inviteRoutes } from './invites';
import { utilityRoutes } from './utility';

const defaultRoute = {
  method: 'GET',
  path: '/',
  options: { cors: true },
  handler: (request: Request, h: ResponseToolkit) => {
      return h.response({message: 'still alive'}).type('text/json').code(200);
  }
}


const routes = [
  defaultRoute,
  ...userRoutes,
  ...categoryRoutes,
  ...forumsRoutes,
  ...threadRoutes,
  ...postRoutes,
  ...adminRoutes,
  ...inviteRoutes,
  ...utilityRoutes,
];

export { routes };