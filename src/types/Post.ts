import { Author } from './Author';

export interface RawPost {
  id: number;
  content: string;
  createdAt: string;
  updatedAt: string;
  authorId?: number;
}

export interface Post {
  id: number;
  content: string;
  createdAt: string;
  updatedAt: string;
  authorId?: number;
  author?: Author;
}

