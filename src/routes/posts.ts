import { Request, ResponseToolkit } from '@hapi/hapi';
import moment from 'moment';
import DataClient from '../data/dataclient';
import { Post, RawPost } from '../types';

import { POSTS_PER_PAGE } from '../constants';
import { getAuthor } from '../utils/GetAuthor';

const transformPosts = (postsRaw: RawPost[] ): Post[] => {
  const posts = postsRaw.map((p) => {
    return {
      id: p.id,
      content: p.content,
      createdAt: p.createdAt,
      updatedAt: p.updatedAt,
      authorId: p.authorId,
    };
  });
  return posts;
};

const postRoutes = [
  {
    method: 'GET',
    path: '/thread/{threadId}/posts',
    options: { cors: true, },
    handler: async (request: Request, h: ResponseToolkit) => {
      const db = await DataClient.create();
      const threadId = request.params.threadId;

      let page = 1;
      let offset = POSTS_PER_PAGE;
      if(request.query.page) {
        page = parseInt(Array.isArray(request.query.page) ? request.query.page[0] : request.query.page);
        offset = (page - 1) * POSTS_PER_PAGE;
      }
      let reply = false;
      if(request.query.r) {
        if(Array.isArray(request.query.r)) {
          if(request.query.r[0] === '1') {
            reply = true;
          }
        }
        else if(request.query.r === '1') {
          reply = true;
        }
      }
      
      const selectFields = {
        id: 'Posts.id',
        content: 'Posts.content',
        createdAt: 'Posts.created_at',
        updatedAt: 'Posts.updated_at',
        authorId: 'Users.id',
      };

      const q = db('Posts').where({ threadId })
        .select(selectFields)
        .leftJoin('Users', 'Users.id', 'Posts.authorId')

      let postsRaw = [];
      if(reply) {
        postsRaw = await q.orderBy('Posts.created_at', 'desc').limit(POSTS_PER_PAGE);
      }
      else {
        postsRaw = await q.orderBy('Posts.created_at', 'asc').offset(offset).limit(POSTS_PER_PAGE);
      }
      
      const posts: Post[] = transformPosts(postsRaw);

      for await (let p of posts) {
        const author = await getAuthor(p.authorId);
        p.author = author;
        delete p.authorId;
      }


      return h.response(posts).type('text/json').code(200);
    },
  },
  {
    method: 'POST',
    path: '/thread/{threadId}/post',
    options: {cors: true },
    handler: async (request: Request, h: ResponseToolkit) => {
      const db = await DataClient.create();
      const threadId = request.params.threadId;
      const { authorId, content } = request.payload as unknown as { authorId: number, content: string };
      const result = await db('Posts').insert({threadId, authorId, content});
      
      return h.response(result).type('text/json').code(200);
    }
  },
  {
    method: 'PUT',
    path: '/thread/{threadId}/post/{id}',
    options: {cors: true },
    handler: async (request: Request, h: ResponseToolkit) => {
      const db = await DataClient.create();
      const id = request.params.id;
      const updated_at = moment().toISOString();
      const { authorId, content } = request.payload as unknown as { authorId: number, content: string };
      const result = await db('Posts').where({id}).update({authorId, content, updated_at});
      
      return h.response({result}).type('text/json').code(200);
    }
  },
];

export { postRoutes };