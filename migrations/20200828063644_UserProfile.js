
exports.up = function(knex) {
  return knex.schema
    .dropTableIfExists('UserProfile')
    .dropTableIfExists('Animals')
    .createTable('Animals', (a) => {
      a.increments();
      a.string('name', 16).notNullable().unique();
      a.integer('popularity').defaultTo(0);
    })
    .dropTableIfExists('Colors')
    .createTable('Colors', (colorsTable) => {
        colorsTable.increments();
        colorsTable.string('name', 16).notNullable().unique();
        colorsTable.integer('popularity').defaultTo(0);
    })
    .dropTableIfExists('Suits')
    .createTable('Suits', (suits) => {
      suits.increments();
      suits.string('name').notNullable().unique();
      suits.integer('popularity').defaultTo(0);
    })
    .dropTableIfExists('Ranks')
    .createTable('Ranks', (ranks) => {
      ranks.increments();
      ranks.string('name').notNullable().unique();
      ranks.integer('popularity').defaultTo(0);
    })
    .dropTableIfExists('Arcana')
    .createTable('Arcana', (arcana) => {
      arcana.increments();
      arcana.integer('index');
      arcana.string('name');
      arcana.integer('popularity').defaultTo(0);
    })
    .dropTableIfExists('UserProfiles')
    .createTable('UserProfiles', (up) => {
      up.increments();
      up.integer('userId').references('id').inTable('Users').unsigned();
      up.string('tagline', 256);
      up.integer('colorId').references('id').inTable('Colors').unsigned();
      up.integer('animalId').references('id').inTable('Animals').unsigned();
      up.string('code', 4);
      up.integer('suitId').references('id').inTable('Suits').unsigned();
      up.integer('rankId').references('id').inTable('Ranks').unsigned();
      up.integer('arcanaId').references('id').inTable('Arcana').unsigned();
      up.integer('rating').defaultTo(0);
      up.boolean('hideEmail').defaultTo(true);
      up.boolean('allowMessages').defaultTo(true);
      up.timestamps();
    })
    .dropTableIfExists('Invitations')
    .createTable('Invitations', (i) => {
      i.increments();
      i.integer('sentBy').references('id').inTable('Users').unsigned();
      i.string('token', 40);
      i.dateTime('sendDate').notNullable().defaultTo(knex.fn.now());
      i.boolean('expired').defaultTo(false);
      i.boolean('used').defaultTo(false);
    })
    .dropTableIfExists('Sessions')
    .createTable('Sessions', function(sessionsTable) {
      sessionsTable.integer('userId').references('id').inTable('Users');     // user id
      sessionsTable.string('session_id', 64);  // guid for the session itself
      sessionsTable.timestamp('started_at').notNullable();
      sessionsTable.timestamp('expires_at').notNullable();
    });;
};

exports.down = function(knex) {
  return knex.schema
    .dropTableIfExists('Sessions')
    .dropTableIfExists('Invitations')
    .dropTableIfExists('UserProfiles')
    .dropTableIfExists('Arcana')
    .dropTableIfExists('Ranks')
    .dropTableIfExists('Suits')
    .dropTableIfExists('Colors')
    .dropTableIfExists('Animals');
};
