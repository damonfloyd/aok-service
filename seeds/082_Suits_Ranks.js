
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('Suits').del().then(() => {
      return knex('Suits').insert([
        {name: 'Swords'},
        {name: 'Rods'},
        {name: 'Coins'},
        {name: 'Cups'},
      ])
      .then(() => {
        return knex('Ranks').del().then(() => {
          return knex('Ranks').insert([
            {name: 'One'},
            {name: 'Two'},
            {name: 'Three'},
            {name: 'Four'},
            {name: 'Five'},
            {name: 'Six'},
            {name: 'Seven'},
            {name: 'Eight'},
            {name: 'Nine'},
            {name: 'Ten'},
            {name: 'Page'},
            {name: 'Knight'},
            {name: 'Queen'},
            {name: 'King'},
            {name: '__ARCANA__'},
          ])
          .then(() => {
            return knex('Arcana').del().then(() => {
              return knex('Arcana').insert([
                { name: 'The Fool', index: 0},
                { name: 'The Magician', index: 1},
                { name: 'The High Priestess', index: 2},
                { name: 'The Empress', index: 3},
                { name: 'The Emperor', index: 4},
                { name: 'The Hierophant', index: 5},
                { name: 'The Lovers', index: 6},
                { name: 'The Chariot', index: 7},
                { name: 'Justice', index: 8},
                { name: 'The Hermit', index: 9},
                { name: 'Wheel Of Fortune', index: 10},
                { name: 'Strength', index: 11},
                { name: 'The Hanged Man', index: 12},
                { name: 'Death', index: 13},
                { name: 'Temperance', index: 14},
                { name: 'The Devil', index: 15},
                { name: 'The Tower', index: 16},
                { name: 'The Star', index: 17},
                { name: 'The Moon', index: 18},
                { name: 'The Sun', index: 19},
                { name: 'Judgement', index: 20},
                { name: 'The World', index: 21},
              ])
            });
          })
        });
      })
    });
};
