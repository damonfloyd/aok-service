import { Request, ResponseToolkit } from '@hapi/hapi';
import DataClient from '../data/dataclient';
import { getAuthor } from '../utils/GetAuthor';



const categoryRoutes = [
  {
    method: 'GET',
    path: '/categories',
    options: { cors: true, auth: { access: { scope: ['user']}} },
    handler: async (request: Request, h: ResponseToolkit) => {
      const userId = parseInt(request.headers['x-aok-user']);
      const db = await DataClient.create();
      const categories = [];
      const cats = await db('Categories').select('*').orderBy('index');
      // community forums are filtered by user profile, so pop this off the stack 
      const community = cats.pop();

      // get subforums for each category (save community)
      for await (let c of cats) {
        const forums = await db('Forums').select('*').where({categoryId: c.id, parentId: null}).orderBy('index');
        for await (let f of forums) {
          const subforums = await db('Forums').select('*').where({parentId: f.id}).orderBy('index');
          const threadCount = (await db('Threads').count('id as count').where({forumId: f.id}))[0];
          const mostRecent = (await db('Threads').where({forumId: f.id}).select('created_at').orderBy('created_at', 'DESC').limit(1))[0];
          f = Object.assign(f, {subforums, threadCount, mostRecent});
        }
        categories.push(Object.assign(c, {forums}));
      }

      // get the user profile 
      const author = await getAuthor(userId);
      // build list of Forum names from it
      const { color, animal, suit, arcana } = author;
      const names = [color, animal, suit, arcana];
      // grab them
      const comForums = await db('Forums').select('*').whereIn('name', names).orderBy('index');
      // and get the subforums
      for await (let cf of comForums) {
        const subforums = await db('Forums').select('*').where({parentId: cf.id}).orderBy('index');
        const threadCount = (await db('Threads').count('id as count').where({forumId: cf.id}))[0];
        const mostRecent = (await db('Threads').where({forumId: cf.id}).select('created_at').orderBy('created_at', 'DESC').limit(1))[0];
        cf = Object.assign(cf, {subforums, threadCount, mostRecent});
      }
      categories.push(Object.assign(community, {forums: comForums}));


      return h.response(categories).type('text/json').code(200);
    }
  }
];

export { categoryRoutes };
