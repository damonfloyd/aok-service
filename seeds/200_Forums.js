
exports.seed = async function(knex, Promise) {
  // Deletes ALL existing entries
  await knex('Forums').del()
  return knex('Forums').insert([
    {id: 1, name: 'Help / Announcements', description: 'You should probably start here', index: 0, hidden: false, inactive: false, categoryId: 1},
    {id: 2, name: 'Introductions', description: 'Introduce your invitees here and let them say hello!', index: 1, hidden: false, inactive: false, categoryId: 1},
    {id: 3, name: 'Being / Nothing', description: 'For anything which doesn\'t fit anywhere else', index: 2, hidden: false, inactive: false, categoryId: 2},
    {id: 4, name: 'Games & Gaming', description: 'Video, board, role-playing or any other kind of game', index: 3, hidden: false, inactive: false, categoryId: 2},
    {id: 5, name: 'Music', description: 'Discover new sounds!  Introduce people to new bands!', index: 4, hidden: false, inactive: false, categoryId: 2},
    {id: 6, name: 'Movies & TV', description: 'What are you watching these days?', index: 5, hidden: false, inactive: false, categoryId: 2},
    {id: 7, name: 'Books', description: 'Reading is FUNdamental!', index: 5, hidden: false, inactive: false, categoryId: 2},
    {id: 8, name: 'Self-Promotion', description: 'wrote a book?  recorded a song?  Tell people about it here!', index: 6, hidden: false, inactive: true, categoryId: 2},
  ]).then( async () => {
    const colors = await knex('Colors').select('*');
    const animals = await knex('Animals').select('*');
    const arcana = await knex('Arcana').select('*');
    const suits = await knex('Suits').select('*').whereNot({name: '__ARCANA__'});

    const EVERYTHING = [...arcana, ...suits, ...colors, ...animals,];
    const defaults = { hidden: false, inactive: true, categoryId: 3};
    let id = 9;
    let index = 7;
    const rows = [];
    for await (let thing of EVERYTHING) {
      const row = Object.assign(defaults, {id, index, name: thing.name});
      await knex('Forums').insert(row);
      id++;
      index++;
    }
  });
};
